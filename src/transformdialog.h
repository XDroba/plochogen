﻿#pragma once
#ifndef TRANSFORMDIALOG_HPP
#define TRANSFORMDIALOG_HPP
#include <QDialog>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>


#include "exprtk.hpp"

typedef exprtk::symbol_table<double> symbol_table_t;
typedef exprtk::expression<double>     expression_t;
typedef exprtk::parser<double>             parser_t;


namespace Ui {class TransformDialog;}

class TransformDialog : public QDialog {
	Q_OBJECT

public:
	TransformDialog(QWidget * parent = Q_NULLPTR, std::vector<int> *selPoints=NULL);
	~TransformDialog();
	bool Transform(vtkSmartPointer<vtkPolyData> polydata);

public slots:
	void ApplyButtonClicked();

signals:
    void apply_clicked();

private:
	Ui::TransformDialog *ui;
	std::vector<int> *selPoints;
};

#endif // TRANSFORMDIALOG_HPP