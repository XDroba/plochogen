#include "mainwindow.h"
#include "ui_mainwindow.h"

//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi)*5-5,x))
//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi*2)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi*2)*5-5,0))
//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi*1.7)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi*1.7)*5-5,x/3.8))

PlochoUI::PlochoUI(QWidget *parent) :
	QMainWindow(parent), transDialog(this,&selPoints),
	ui(new Ui::PlochoUI)
{
	ui->setupUi(this);
	this->setWindowTitle("PlochoGen");

	QAction *helpAction = ui->menuBar->addAction("Transform");
	connect(helpAction, SIGNAL(triggered()), this, SLOT(transform()));

	connect(&transDialog, &TransformDialog::apply_clicked, this, &PlochoUI::ApplyTransform);


	//vtkObject::GlobalWarningDisplayOff();

	//incializacia vtk rendera
	renderer = vtkSmartPointer<vtkRenderer>::New();
	QSurfaceFormat surfaceFormat = ui->qvtkWidget->windowHandle()->format();
	surfaceFormat.setSamples(8);
	surfaceFormat.setStencilBufferSize(8);
	ui->qvtkWidget->windowHandle()->setFormat(surfaceFormat);

	ui->qvtkWidget->GetRenderWindow()->AddRenderer(renderer);

	plochoInteractorStyle = vtkSmartPointer<PlochoInteractorStyle>::New();
	ui->qvtkWidget->GetRenderWindow()->GetInteractor()->SetInteractorStyle(plochoInteractorStyle);
	plochoInteractorStyle->init(renderer, &selPoints);
	ui->qvtkWidget->GetRenderWindow()->SetMultiSamples(8);
	renderer->ResetCamera();
	renderer->SetBackground(0.2, 0.3, 0.4);

	axes = vtkSmartPointer<vtkAxesActor>::New();
	widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	widget->SetOrientationMarker(axes);
	widget->SetInteractor(ui->qvtkWidget->GetRenderWindow()->GetInteractor());
	widget->SetViewport(0.0, 0.0, 0.1, 0.2);
	widget->SetEnabled(1);
	widget->InteractiveOff();
	ui->qvtkWidget->update();
	connect(plochoInteractorStyle.GetPointer(), SIGNAL(UpdateWidget()), this, SLOT(UpdateWidget()));
}
//destruktor
PlochoUI::~PlochoUI()
{
	//ulozime si polohu a velkost okna
	if (!this->isMaximized())
	{
		settings.setValue("window_width", this->width());
		settings.setValue("window_height", this->height());
		settings.setValue("window_posX", this->pos().x());
		settings.setValue("window_posY", this->pos().y());
	}
	settings.setValue("maximized", this->isMaximized());
	settings.sync();
    delete ui;
}

//vola sa, ked sa ma zobrazit hlavne okno
void PlochoUI::show()
{
	QWidget::show();
	//nastavime velkost a polohu okna podla ulozenych hodnot z minula
	this->resize(QSize(settings.value("window_width", this->width()).toInt(), settings.value("window_height", this->height()).toInt()));
	this->move(settings.value("window_posX", this->pos().x()).toInt(), settings.value("window_posY", this->pos().y()).toInt());
	if (settings.value("maximized", false).toBool())this->showMaximized();
}

//resetovanie kamery zobrazenia
void PlochoUI::reset_viewport_clicked()
{
	this->renderer->ResetCamera();
	this->ui->qvtkWidget->update();
}

void   PlochoUI::closeEvent(QCloseEvent*)
{
	qApp->quit();
}

void PlochoUI::color_object()
{
	if (actor == NULL)return;
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
	vtkSmartPointer<vtkUnsignedCharArray> colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
	colors->SetNumberOfComponents(3);
	colors->SetName("Colors");
	size_t pocet = polydata->GetNumberOfPoints();
	colors->SetNumberOfTuples(pocet);
	unsigned char farba[3];
	double rgb[3];
	vtkSmartPointer<vtkColorTransferFunction> color = vtkSmartPointer<vtkColorTransferFunction>::New();
	color->AddRGBPoint(0.0, 0.0, 0.0, 0.5);
	color->AddRGBPoint(0.1, 0.0, 0.0, 1.0);
	color->AddRGBPoint(0.35, 0.0, 1.0, 1.0);
	color->AddRGBPoint(0.65, 1.0, 1.0, 0.0);
	color->AddRGBPoint(0.9, 1.0, 0.0, 0.0);
	color->AddRGBPoint(1.0, 0.5, 0.0, 0.0);

	for (int i = 0; i < pocet; i++)
	{
		color->GetColor(i / (double)pocet, rgb);
		farba[0] = rgb[0] * 255;
		farba[1] = rgb[1] * 255;
		farba[2] = rgb[2] * 255;
		colors->SetTypedTuple(i, farba);
	}
	polydata->GetPointData()->SetScalars(colors);
	polydata->Modified();
}


//ukoncenie
void PlochoUI::exit_clicked()
{
	this->close();
}

void PlochoUI::UpdateWidget()
{
	this->ui->qvtkWidget->update();
}

void PlochoUI::create_torus()
{
	create_object(tvar::torus);
}

void PlochoUI::create_plane()
{
	create_object(tvar::plane);
}

void PlochoUI::create_sphere()
{
	create_object(tvar::sphere);
}

void PlochoUI::transform()
{
	if (actor == NULL)return;
	transDialog.show();
	transDialog.raise();
	transDialog.activateWindow();
}

void PlochoUI::ApplyTransform()
{
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
	std::cout << "Total number of points: " << polydata->GetNumberOfPoints() << std::endl;
	if (transDialog.Transform(polydata) == false)return;
	polydata->Modified();
	
	
	actor->GetMapper()->SetInputConnection(CalculateNormals(actor->GetMapper()->GetInputAlgorithm()->GetOutputPort()));
	renderer->ResetCamera();
	this->UpdateWidget();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
}

void PlochoUI::LinearDiv()
{
	Subdivide(0);
}

void PlochoUI::LoopDiv()
{
	Subdivide(1);
}

void PlochoUI::ButterDiv()
{
	Subdivide(2);
}

void PlochoUI::DecPro()
{
	Decimate(0);
}

void PlochoUI::DecQuad()
{
	Decimate(1);
}

void PlochoUI::DecClus()
{
	Decimate(2);
}

void PlochoUI::SmoothLap()
{
	Smooth(0);
}

void PlochoUI::SmoothWin()
{
	Smooth(1);
}

void PlochoUI::Decimate(int which)
{
	if (actor == NULL)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> decimationFilter;
	switch (which)
	{
	case 0:
		decimationFilter = vtkSmartPointer<vtkDecimatePro>::New();
		dynamic_cast<vtkDecimatePro *> (decimationFilter.GetPointer())->SetTargetReduction(0.5);
		break;
	case 1:
		decimationFilter = vtkSmartPointer<vtkQuadricDecimation>::New();
		break;
	case 2:
		decimationFilter = vtkSmartPointer<vtkQuadricClustering>::New();
		break;
	default:
		break;
	}


	decimationFilter->SetInputData(actor->GetMapper()->GetInput());
	decimationFilter->Update();

	actor->GetMapper()->SetInputConnection(CalculateNormals(decimationFilter->GetOutputPort()));
	this->UpdateWidget();
}

void PlochoUI::Smooth(int which)
{
	if (actor == NULL)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> smoothFilter;
	switch (which)
	{
	case 0:
		smoothFilter = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
		break;
	case 1:
		smoothFilter = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
		dynamic_cast<vtkWindowedSincPolyDataFilter *> (smoothFilter.GetPointer())->BoundarySmoothingOff();
		break;
	default:
		break;
	}
	smoothFilter->SetInputData(actor->GetMapper()->GetInput());
	smoothFilter->Update();
	
	actor->GetMapper()->SetInputConnection(CalculateNormals(smoothFilter->GetOutputPort()));
	this->UpdateWidget();
}

void PlochoUI::create_object(tvar which)
{
	TvaroDialog *tvardialog;
	switch (which)
	{
	case tvar::plane:
		tvardialog = &pdialog;
		break;
	case tvar::sphere:
		tvardialog = &sdialog;
		break;
	case tvar::torus:
		tvardialog = &tdialog;
		break;
	default:
		return;
	}
	if (tvardialog->exec() == QDialog::Rejected)return;
	renderer->RemoveAllViewProps();
	actor = tvardialog->GetActor();
	renderer->AddActor(actor);
	renderer->ResetCamera();
	plochoInteractorStyle->ClearPoints();
	this->UpdateWidget();
}

vtkAlgorithmOutput * PlochoUI::CalculateNormals(vtkAlgorithmOutput * data)
{
	if (!computeNormals)
	{
		actor->GetProperty()->SetInterpolationToFlat();
		return data;
	}
	normalGenerator = vtkSmartPointer<vtkPolyDataNormals>::New();
	normalGenerator->SetInputConnection(data);
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->AutoOrientNormalsOn();
	normalGenerator->ConsistencyOn();
	normalGenerator->NonManifoldTraversalOn();
	normalGenerator->ComputeCellNormalsOff();
	normalGenerator->SplittingOff();
	normalGenerator->Update();
	return normalGenerator->GetOutputPort();
}

void PlochoUI::Subdivide(int which)
{
	if (actor == NULL)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> subdivisionFilter;
	switch (which)
	{
	case 0:
		subdivisionFilter = vtkSmartPointer<vtkLinearSubdivisionFilter>::New();
		dynamic_cast<vtkLinearSubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	case 1:
		subdivisionFilter = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
		dynamic_cast<vtkLoopSubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	case 2:
		subdivisionFilter = vtkSmartPointer<vtkButterflySubdivisionFilter>::New();
		dynamic_cast<vtkButterflySubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	default:
		break;
	}
	
	subdivisionFilter->SetInputData(actor->GetMapper()->GetInput());
	subdivisionFilter->Update();
	actor->GetMapper()->SetInputConnection(CalculateNormals(subdivisionFilter->GetOutputPort()));
	this->UpdateWidget();
}


