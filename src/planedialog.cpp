﻿#include "planedialog.h"
#include "ui_planedialog.h"

PlaneDialog::PlaneDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::PlaneDialog();
	ui->setupUi(this); 
}

vtkSmartPointer<vtkActor> PlaneDialog::GetActor()
{
	vtkSmartPointer<vtkPlaneSource> plane = vtkSmartPointer<vtkPlaneSource>::New();
	plane->SetOrigin(ui->spinXstart->value(), ui->spinYstart->value(), 0);
	plane->SetPoint1(ui->spinXend->value(), ui->spinYstart->value(), 0);
	plane->SetPoint2(ui->spinXstart->value(), ui->spinYend->value(), 0);
	plane->SetXResolution(ui->spinXres->value());
	plane->SetYResolution(ui->spinYres->value());

	vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
	triangulate->SetInputConnection(plane->GetOutputPort());
	triangulate->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(triangulate->GetOutputPort());
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	//actor->GetProperty()->SetInterpolationToFlat();
	return actor;
}

PlaneDialog::~PlaneDialog() {
	delete ui;
}
