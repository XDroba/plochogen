﻿#include "torusdialog.h"
#include "ui_torusdialog.h"

TorusDialog::TorusDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::TorusDialog();
	ui->setupUi(this); 
	this->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

vtkSmartPointer<vtkActor> TorusDialog::GetActor()
{
	vtkSmartPointer<vtkParametricFunctionSource> torusSource = vtkSmartPointer<vtkParametricFunctionSource>::New();
	vtkSmartPointer<vtkParametricTorus> torus = vtkSmartPointer<vtkParametricTorus>::New();
	torus->SetRingRadius(ui->ringRadiusSpin->value());
	torus->SetCrossSectionRadius(ui->crossSectSpin->value());
	torus->JoinUOff();
	torus->JoinVOff();
	torus->JoinWOff();
	torusSource->SetParametricFunction(torus);
	torusSource->SetUResolution(ui->uResSpin->value());
	torusSource->SetVResolution(ui->vResSpin->value());
	torusSource->SetWResolution(ui->vResSpin->value());
	//torusSource->GenerateTextureCoordinatesOff();
	//torusSource->GenerateNormalsOff();
	torusSource->Update();

	/*vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
	featureEdges->FeatureEdgesOff();
	featureEdges->BoundaryEdgesOff();
	featureEdges->NonManifoldEdgesOn();
	featureEdges->SetInputConnection(torusSource->GetOutputPort());
	featureEdges->Update();

	int numberOfOpenEdges = featureEdges->GetOutput()->GetNumberOfCells();
	std::cout << numberOfOpenEdges << std::endl;*/
	/*vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(40.0);
	sphere->SetPhiResolution(ui->uResSpin->value());
	sphere->SetThetaResolution(ui->vResSpin->value());*/

	/*vtkSmartPointer<vtkPlaneSource> sphere = vtkSmartPointer<vtkPlaneSource>::New();
	sphere->SetOrigin(0, 0, 0);
	sphere->SetPoint1(10, 0, 0);
	sphere->SetPoint2(0, 10, 0);
	sphere->SetXResolution(10);
	sphere->SetYResolution(10);*/
	
	/*vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
	writer->SetFileName("torus.vtk");
	writer->SetInputConnection(torusSource->GetOutputPort());
	writer->SetFileTypeToASCII();
	writer->Write();*/
	/*vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
	reader->SetFileName("torus.vtk");
	reader->Update();*/

	vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
	triangulate->SetInputConnection(torusSource->GetOutputPort());
	triangulate->Update();

	/*featureEdges->SetInputConnection(triangulate->GetOutputPort());
	featureEdges->Update();

	numberOfOpenEdges = featureEdges->GetOutput()->GetNumberOfCells();
	std::cout << numberOfOpenEdges << std::endl;*/

	/*vtkSmartPointer<vtkStripper> stripper =
		vtkSmartPointer<vtkStripper>::New();
	stripper->SetInputConnection(triangulate->GetOutputPort());
	stripper->Update();

	featureEdges->SetInputConnection(stripper->GetOutputPort());
	featureEdges->Update();

	numberOfOpenEdges = featureEdges->GetOutput()->GetNumberOfCells();
	std::cout << numberOfOpenEdges << std::endl;*/

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(triangulate->GetOutputPort());
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	//actor->GetProperty()->SetInterpolationToFlat();
	return actor;
}

TorusDialog::~TorusDialog() {
	delete ui;
}

