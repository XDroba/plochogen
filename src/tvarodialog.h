﻿#pragma once
#include <QDialog>
#include <vtkActor.h>
#include <vtkSmartPointer.h>
namespace Ui {class TvaroDialog;}

class TvaroDialog : public QDialog {
	Q_OBJECT

public:
	TvaroDialog(QDialog * parent = Q_NULLPTR);
	virtual vtkSmartPointer<vtkActor> GetActor()=0;
	virtual ~TvaroDialog();

protected:
	vtkSmartPointer<vtkActor> actor;

};
