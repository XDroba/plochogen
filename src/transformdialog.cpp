﻿#include "transformdialog.h"
#include "ui_transformdialog.h"

TransformDialog::TransformDialog(QWidget * parent, std::vector<int> *selPoints) : QDialog(parent) {
	ui = new Ui::TransformDialog();
	ui->setupUi(this); 
	this->selPoints = selPoints;
}

TransformDialog::~TransformDialog() {
	delete ui;
}

void TransformDialog::ApplyButtonClicked()
{
	emit apply_clicked();
}

bool TransformDialog::Transform(vtkSmartPointer<vtkPolyData> polydata)
{
	std::string expression_string1 = ui->xLine->text().toStdString();
	std::string expression_string2 = ui->yLine->text().toStdString();
	std::string expression_string3 = ui->zLine->text().toStdString();
	double bod[3];
	symbol_table_t symbol_table;
	symbol_table.add_variable("x", bod[0]);
	symbol_table.add_variable("y", bod[1]);
	symbol_table.add_variable("z", bod[2]);
	symbol_table.add_constants();
	expression_t expression1;
	expression1.register_symbol_table(symbol_table);
	expression_t expression2;
	expression2.register_symbol_table(symbol_table);
	expression_t expression3;
	expression3.register_symbol_table(symbol_table);

	parser_t parser;
	if (!parser.compile(expression_string1, expression1)) {
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (!parser.compile(expression_string2, expression2)){
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (!parser.compile(expression_string3, expression3)){
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (selPoints->size() == 0)
	{
		for (int i = 0; i < polydata->GetNumberOfPoints(); i++)
		{
			polydata->GetPoint(i, bod);
			double x = expression1.value();
			double y = expression2.value();
			double z = expression3.value();
			polydata->GetPoints()->SetPoint(i, x, y, z);
			//std::cout << bod[0] << " " << bod[1] << " " << bod[2] << std::endl;
		}
	}
	else
	{
		for (int i = 0; i < selPoints->size(); i++)
		{
			polydata->GetPoint((*selPoints)[i], bod);
			double x = expression1.value();
			double y = expression2.value();
			double z = expression3.value();
			polydata->GetPoints()->SetPoint((*selPoints)[i], x, y, z);
			//std::cout << bod[0] << " " << bod[1] << " " << bod[2] << std::endl;
		}
	}
	return true;
}
