﻿#include "spheredialog.h"
#include "ui_spheredialog.h"

SphereDialog::SphereDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::SphereDialog();
	ui->setupUi(this); 
}

vtkSmartPointer<vtkActor> SphereDialog::GetActor()
{
	vtkSmartPointer<vtkSphereSource> sphere = vtkSmartPointer<vtkSphereSource>::New();
	sphere->SetRadius(ui->radSpin->value());
	sphere->SetPhiResolution(ui->phiSpin->value());
	sphere->SetThetaResolution(ui->thetaSpin->value());

	vtkSmartPointer<vtkTriangleFilter> triangulate = vtkSmartPointer<vtkTriangleFilter>::New();
	triangulate->SetInputConnection(sphere->GetOutputPort());
	triangulate->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(triangulate->GetOutputPort());
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	//actor->GetProperty()->SetInterpolationToFlat();
	return actor;
}

SphereDialog::~SphereDialog() {
	delete ui;
}
